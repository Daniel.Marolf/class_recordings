This program was designed to take Zoom recordings of class sessions that were automatically saved to a common folder in Google Drive and move those recordings to folders that were already shared with my various classes. This would enable absent or distance students to watch the lecture and save me the hassle of having to filter through which recordings needed to go to which folder.

Requirements:
- Google Drive API access and the creation of an OAuth Client ID associated with the email address of the Google Drive you are working within
    - I wasn't able to have Google API access with my school email, so my workaround was to create a personal gmail account and give edit access of the class recordings folder to my personal gmail account
- settings.yaml file
    - I found that at least for moving folders/files I needed to add some oauth scopes. If you intend to do more than just move them, you may need more. More information can be found here: https://pythonhosted.org/PyDrive/oauth.html


For more information about moving files/folders in Google Drive, see the links below. They are what I used primarily to figure this out.
- https://stackoverflow.com/questions/43482895/cannot-move-a-file-across-folders-in-google-drive-using-pydrive
- https://stackoverflow.com/questions/34101427/accessing-folders-subfolders-and-subfiles-using-pydrive-python
- https://pythonhosted.org/PyDrive/
