import os, re
from oauth2client.client import HttpAccessTokenRefreshError
from pydrive.auth import GoogleAuth, RefreshError
from pydrive.drive import GoogleDrive

os.chdir('/Users/username/path_to_json/credentials.json')

"""
Need IDs for original_folder and destination_folder(s)
"""
original_folder = 'original_folder ID'
destination_8th = 'destination_8th ID' #destination folders named after my class sections
destination_7th = 'destination_7th ID'
destination_6th = 'destination_6th ID'
destination_4th = 'destination_4th ID'
destination_3rd = 'destination_3rd ID'
destination_2nd = 'destination_2nd ID'

"""
Class sessions recorded through Zoom and automatically named based on the Zoom meeting name, which always included '_th period' (or _nd or _rd).
Regex below searches the folder names to figure out what the correct destination folder should be.
"""
regex_title = re.compile(r'- (\d\w{2}) period') 

def regex_title_search(file_title):
    period = regex_title.findall(file_title)
    if len(period) == 0:
        return original_folder
    elif period[0] == '8th':
        return destination_8th
    elif period[0] == '7th':
        return destination_7th
    elif period[0] == '6th':
        return destination_6th
    elif period[0] == '4th':
        return destination_4th
    elif period[0] == '3rd':
        return destination_3rd
    elif period[0] == '2nd':
        return destination_2nd

def move_folder(file_id, new_parent_id):
    new_parent_info = [{'kind': 'drive#parentReference', 'id': new_parent_id, 'selfLink': f'https://www.googleapis.com/drive/v2/files/{file_id}/parents/{new_parent_id}', 'parentLink': f'https://www.googleapis.com/drive/v2/files/{new_parent_id}', 'isRoot': False}]
    folder = drive.CreateFile({'id':file_id})
    folder['parents'] # This line is required. It will not work if removed. Not entirely sure why.
    folder['parents'] = new_parent_info
    folder.Upload()

def connect_to_drive():
    gauth = GoogleAuth()
    gauth.LocalWebserverAuth()
    drive = GoogleDrive(gauth)
    return drive

def create_file_list(drive):
    file_list = drive.ListFile({'q': f'"{original_folder}" in parents'}).GetList()
    return file_list


try:
    drive = connect_to_drive()
    file_list = create_file_list(drive)
    for f in file_list:
        file_id = f['id']
        file_title = f['title']
        new_parent_id = regex_title_search(file_title)
        move_folder(file_id, new_parent_id)
    print('Folders moved.')
except (RefreshError, HttpAccessTokenRefreshError):
    # just in case the token has expired and can't be refreshed without manual authentication
    if os.path.exists('credentials.json'):
        os.remove('credentials.json')
        drive = connect_to_drive()
        file_list = create_file_list(drive)
        for f in file_list:
            file_id = f['id']
            file_title = f['title']
            new_parent_id = regex_title_search(file_title)
            move_folder(file_id, new_parent_id)
        print('Folders moved.')
    else:
        print('No file of that name in the path.')

